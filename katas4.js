const gotCitiesCSV =
  "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = [
  "Mordor",
  "Gondor",
  "Rohan",
  "Beleriand",
  "Mirkwood",
  "Dead Marshes",
  "Rhun",
  "Harad"
];

const bestThing =
  "The best thing about a boolean is even if you are wrong you are only off by a bit";

function displayKatas(kata) {
  let breakElement = document.createElement("br");
  let newElement = document.createElement("div");
  newElement.textContent = JSON.stringify(kata);
  document.body.appendChild(newElement);
  document.body.appendChild(breakElement);
  console.log(kata);
  return kata;
}

function kata1() {
  const gotCitiesCSVArr = gotCitiesCSV.split(",");
  return gotCitiesCSVArr;
}

function kata2() {
  const bestThingArr = bestThing.split(" ");
  return bestThingArr;
}

function kata3() {
  const gotCitiesCSVSemiString = gotCitiesCSV.split(",").join(";");
  return gotCitiesCSVSemiString;
}

function kata4() {
  const lotrCitiesArrayToString = lotrCitiesArray.toString();
  return lotrCitiesArrayToString;
}

function kata5() {
  const lotrCitiesArrayTo5 = [];
  for (let i = 0; i < 5; i++) {
    lotrCitiesArrayTo5.push(lotrCitiesArray[i]);
  }
  return lotrCitiesArrayTo5;
}

function kata6() {
  const lotrCitiesArrayLast5 = [];
  for (
    let i = lotrCitiesArray.length - 1;
    i > lotrCitiesArray.length - 6;
    i--
  ) {
    lotrCitiesArrayLast5.push(lotrCitiesArray[i]);
  }
  return lotrCitiesArrayLast5;
}

function kata7() {
  const lotrCitiesArray3To5 = [];
  for (let i = 0; i < 5; i++) {
    if (i >= 2) {
      lotrCitiesArray3To5.push(lotrCitiesArray[i]);
    }
  }
  return lotrCitiesArray3To5;
}

function kata8() {
  const noRohan = lotrCitiesArray;
  noRohan.splice(2, 1);
  return noRohan;
}

function kata9() {
  const noCitiesAfterDM = lotrCitiesArray;

  for (let i = 0; i < noCitiesAfterDM.length; i++) {
    noCitiesAfterDM.splice(5, 1);
  }

  return noCitiesAfterDM;
}

function kata10() {
  const addRohanBack = lotrCitiesArray;
  addRohanBack.splice(2, 0, "Rohan");
  return addRohanBack;
}

function kata11() {
  const renameDeadMarhes = lotrCitiesArray;
  renameDeadMarhes.splice(5, 1, "Deadest Marshes");
  return renameDeadMarhes;
}

function kata12() {
  return bestThing.slice(0, 15);
}

function kata13() {
  return bestThing.slice(-12);
}

function kata14() {
  return bestThing.slice(23, 38);
}

function kata15() {
  return bestThing.substring(-12);
}

function kata16() {
  return bestThing.substring(23, 38);
}

function kata17() {
  return bestThing.indexOf("only");
}

function kata18() {
  return bestThing.lastIndexOf("bit");
}

function kata19() {
  const gotCitiesCSVArr = gotCitiesCSV.split(",");
  const vowels = ["a", "e", "i", "o", "u"];
  const doubleVowels = ["aa", "ee", "ii", "oo", "uu"];
  const doubleVowelsArr = [];

  for (let i = 0; i < gotCitiesCSVArr.length; i++) {
    let currentItem = gotCitiesCSVArr[i];
    let doubleHolder = [];
    let counter = 0;

    for (let j = 0; j < currentItem.length; j++) {
      if (vowels.includes(currentItem[j])) {
        doubleHolder.push(currentItem[j]);
      }
    }

    console.log(doubleHolder);

    if (doubleVowels.includes(doubleHolder)) {
      doubleVowelsArr.push(currentItem);
    }
  }

  return doubleVowelsArr;
}

function kata20() {
  const orArr = [];

  for (let i = 0; i < lotrCitiesArray.length; i++) {
    if (lotrCitiesArray[i].includes("or")) {
      orArr.push(lotrCitiesArray[i]);
    }
  }

  return orArr;
}

function kata21() {
  const bestThingArr = bestThing.split(" ");
  const startWithB = [];

  for (let i = 0; i < bestThingArr.length; i++) {
    if (bestThingArr[i].startsWith("b")) {
      startWithB.push(bestThingArr[i]);
    }
  }

  return startWithB;
}

function kata22() {
  if (lotrCitiesArray.includes("Mirkwood")) {
    return "Yes";
  } else {
    return "No";
  }
}

function kata23() {
  if (lotrCitiesArray.includes("Hollywood")) {
    return "Yes";
  } else {
    return "No";
  }
}

function kata24() {
  return lotrCitiesArray.indexOf("Mirkwood");
}

function kata25() {
  const moreThan1Word = [];
  for (let i = 0; i < lotrCitiesArray.length; i++) {
    let currentItem = lotrCitiesArray[i];
    let spaceCount = 0;

    for (let j = 0; j < currentItem.length; j++) {
      if (currentItem[j] === " ") {
        spaceCount = spaceCount + 1;
      }
    }

    if (spaceCount > 0) {
      moreThan1Word.push(currentItem);
    }
  }

  return moreThan1Word;
}

function kata26() {
  let lotrCitiesArrayCopy = lotrCitiesArray.slice(0);
  const reverseLotrCitiesArray = [];
  const arrayLength = lotrCitiesArray.length;

  for (let i = 0; i < arrayLength; i++) {
    reverseLotrCitiesArray.push(lotrCitiesArrayCopy.pop());
  }

  return reverseLotrCitiesArray;
}

function kata27() {
  return lotrCitiesArray.sort();
}

function kata28() {
  const lotrCitiesArrayCopy = lotrCitiesArray.slice(0);

  for (let i = 0; i < lotrCitiesArrayCopy.length; i++) {
    let temp = lotrCitiesArrayCopy[i];
    let j = i - 1;

    while (j >= 0 && lotrCitiesArrayCopy[j].length > temp.length) {
      lotrCitiesArrayCopy[j + 1] = lotrCitiesArrayCopy[j];
      j--;
    }
    lotrCitiesArrayCopy[j + 1] = temp;
  }

  return lotrCitiesArrayCopy;
}

function kata29() {
  lotrCitiesArray.pop();
  return lotrCitiesArray;
}

function kata30() {
  lotrCitiesArray.push("Rohan");
  return lotrCitiesArray;
}

function kata31() {
  lotrCitiesArray.shift();
  return lotrCitiesArray;
}

function kata32() {
  lotrCitiesArray.unshift("Beleriand");
  return lotrCitiesArray;
}
displayKatas(kata1());
displayKatas(kata2());
displayKatas(kata3());
displayKatas(kata4());
displayKatas(kata5());
displayKatas(kata6());
displayKatas(kata7());
displayKatas(kata8());
displayKatas(kata9());
displayKatas(kata10());
displayKatas(kata11());
displayKatas(kata12());
displayKatas(kata13());
displayKatas(kata14());
displayKatas(kata15());
displayKatas(kata16());
displayKatas(kata17());
displayKatas(kata18());
displayKatas(kata19());
displayKatas(kata20());
displayKatas(kata21());
displayKatas(kata22());
displayKatas(kata23());
displayKatas(kata24());
displayKatas(kata25());
displayKatas(kata26());
displayKatas(kata27());
displayKatas(kata28());
displayKatas(kata29());
displayKatas(kata30());
displayKatas(kata31());
displayKatas(kata32());
